import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Hero } from './hero';
import { HeroService } from './hero.service';

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css' ]
})

export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];

  myName: string = 'TestName';
  myAge: number = 10;
  myMarks1: number = 40;
  myMarks2: number = 60;

  showStringInterpolation = true;
  showPB = false;
  showEB = false;
  showTwoWay = false;


  canUpdate = false;
  canCreate = true;
  canDelete = 'true';


  textValue = 'Sample text value' ;

  imgSrc = 'https://www.google.co.in/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';

  hrefValue = 'https://www.w3schools.com/html/html_attributes.asp';

  pStyle = 'green';
  pFontStyle = 'white';

  constructor(
    private router: Router,
    private heroService: HeroService) {
    
  }

  ngOnInit(): void {
    this.heroService.getHeroes()
      .then(heroes => this.heroes = heroes.slice(1, 5));

      this.showPropertyBinding();
  }

  gotoDetail(hero: Hero): void {
    const link = ['/detail', hero.id];
    this.router.navigate(link);
  }

  myMethod(){
    return 'Value returned by myMethod()';
  }

  showSI(){
    this.showStringInterpolation = true;
    this.showPB = false;
    this.showEB = false;
    this.showTwoWay = false;    
  }

  showPropertyBinding(){
    this.showStringInterpolation = false;
    this.showPB = true;
    this.showEB = false;
    this.showTwoWay = false;    
  }

  showEventBinding(){
    this.showStringInterpolation = false;
    this.showPB = false;
    this.showEB = true;
    this.showTwoWay = false;    
  }

  showTwoWayBinding(){
    this.showStringInterpolation = false;
    this.showPB = false;
    this.showEB = false;
    this.showTwoWay = true;
  }      
}